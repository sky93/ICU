google.load("visualization", "1", {packages:["corechart"]});
google.setOnLoadCallback(drawChart);
var systolic;
var diastolic;
var blood_time;
var saveData = 0;
var day_change = 0;
var lastBloodtime = [-1];
var lastHearttime = [-1];
var time = new Date();
var year = time.getFullYear();
var mon = time.getMonth();
var day = time.getDate();
//////////////WTF!!!!!!!/////Each Time event call in work 2 times like a for loop check is for to control event work just one time////
var checkEventbloodrepat = 2;
var checkEventheartrepat = 2;
//////Check If the Blood or heart for the first time have problem or not
var checkBloodpressure = 1;
var checkHeartbeat = 1;
//////////////////////////////////////////////////////////////////////////////////////////
function drawChart() {
    var chart = new google.visualization.LineChart(document.querySelector('#chart_div'));
    var data = new google.visualization.DataTable();
    data.addColumn('datetime','Timeline'); // Implicit domain label col.
    data.addColumn('number','Surgery Time Start');
    data.addColumn('number','Blood Pressure');
    data.addColumn('number','Heart Beat');
    $(window).resize(function(){
        chart.draw(data, {
        pointSize: 5
        });
    });
    $("#save-blood-pressure-duration").click(function(){
        if(checkEventbloodrepat %  2 == 0){
            //////////////////////blood pressure validation
            var dismiss_check=0;
            if($("#blood-pressure-start").val() == ""){
                $("#save-blood-pressure-duration").attr("data-dismiss","");
                $("#blood-pressure-start-div").attr("class","form-group row has-error");
                $("#blood-pressure-start-error").attr("class","help-block");
            }
            else{
                dismiss_check++;
                $("#blood-pressure-start-div").attr("class","form-group row");
                $("#blood-pressure-start-error").attr("class","help-block list-hide");
            }
            if($("#blood-pressure-end").val() == ""){
                $("#save-blood-pressure-duration").attr("data-dismiss","");
                $("#blood-pressure-end-div").attr("class","form-group row has-error");
                $("#blood-pressure-end-error").attr("class","help-block");
            }
            else{
                $("#blood-pressure-end-div").attr("class","form-group row");
                $("#blood-pressure-end-error").attr("class","help-block list-hide");
                dismiss_check++;
            }
            if(dismiss_check == 2){
                $("#save-blood-pressure-duration").attr("data-dismiss","modal");
                    systolic = $("#blood-pressure-start").val();
                    diastolic = $("#blood-pressure-end").val();
                    blood_time = $("#blood-pressure-time").val().split(":");
                    $('<input>').attr({
                        type: 'hidden',
                        name: 'systolic',
                        value: systolic
                    }).appendTo('form');
                    $('<input>').attr({
                        type: 'hidden',
                        name: 'diastolic',
                        value: diastolic
                    }).appendTo('form');
                    $('<input>').attr({
                        type: 'hidden',
                        name: 'blood_pressure_time',
                        value: blood_time
                    }).appendTo('form');
                    if(checkBloodpressure == 1 && checkHeartbeat == 1){
                        var startTime = startSurgerytime();
                        lastBloodtime[0] = startTime[1];
                        if(startTime[0] != undefined && startTime[1] != undefined){
                            data.addRows([
                                [new Date(year,mon,day,startTime[0],startTime[1]),0,null,null],
                                [new Date(year,mon,day,startTime[0],startTime[1]),0,null,null],
                                [null, null,null,null],
                                [new Date(year,mon,day,startTime[0],startTime[1]),0,null,null],
                                [new Date(year,mon,day,startTime[0],startTime[1]),0,null,null]
                            ]);
                            toastr.success('چارت با موفقیت رسم شد !');
                            checkBloodpressure = 0;
                        }
                        else{
                            toastr.error('زمان شروع و پایان عمل مشخص نیست !');
                            checkBloodpressure = 1;
                        }
                    }
                    if(checkBloodpressure == 0 || checkHeartbeat == 0){
                        if(lastBloodtime[0] != undefined){
                            if(lastBloodtime[0]>blood_time[0]){
                                day_change = 1;
                            }
                        }
                        data.addRows([
                            [new Date(year,mon,day+day_change,parseInt(blood_time[0]),parseInt(blood_time[1]),0),null,parseInt(diastolic),null],
                            [new Date(year,mon,day+day_change,parseInt(blood_time[0]),parseInt(blood_time[1]),0),null,parseInt(systolic),null],
                            [null, null,null,null]
                        ]);
                        chart.draw(data, {
                            pointSize: 5
                        });
                    }
                saveData++;
                }
            }
        checkEventbloodrepat++;
    });
    $("#choseـheart_beat").click(function(){
        if(checkEventheartrepat % 2 == 0){
            if($("#heart_beat_time").val() == "" || $("#heart_beat_number").val() == "" || isNaN($("#heart_beat_number").val()) ){
                $("#heart_beat_valid").attr("class","help-block");
                $("#heart_beat_div").attr("class","form-group row has-error");
                $("#heart_beat_number_valid").attr("class","help-block");
                $("#choseـheart_beat").attr("data-dismiss","");
            }
            else{
                $("#choseـheart_beat").attr("data-dismiss","modal");
                var heartBeat = $("#heart_beat_number").val();
                var heartTime = $("#heart_beat_time").val().split(":");
                $('<input>').attr({
                    type: 'hidden',
                    name: 'heartBeat',
                    value: heartBeat
                }).appendTo('form');
                $('<input>').attr({
                    type: 'hidden',
                    name: 'heartTime',
                    value: heartTime
                }).appendTo('form');
                if(checkBloodpressure == 1 && checkHeartbeat == 1){
                    var startTime = startSurgerytime();
                    lastHearttime[0] = startTime[1];
                    if(startTime[0] != undefined && startTime[1] != undefined){
                        data.addRows([
                            [new Date(year,mon,day,startTime[0],startTime[1]),0,null,null],
                            [new Date(year,mon,day,startTime[0],startTime[1]),0,null,null],
                            [null, null,null,null],
                            [new Date(year,mon,day,startTime[0],startTime[1]),0,null,null],
                            [new Date(year,mon,day,startTime[0],startTime[1]),0,null,null]
                        ]);
                        chart.draw(data, {
                            pointSize: 5
                        });
                        toastr.success('چارت با موفقیت رسم شد !');
                        checkHeartbeat = 0;
                    }
                    else{
                        toastr.error('زمان شروع و پایان عمل مشخص نیست !');
                        checkHeartbeat = 1;
                    }
                }
                if(checkHeartbeat == 0 || checkBloodpressure == 0){
                    if(lastHearttime[0] != undefined){
                        if(lastHearttime[0]>heartTime[0]){
                            day_change = 1;
                        }
                    }
                    data.addRows([
                        [new Date(year,mon,day+day_change,parseInt(heartTime[0]),parseInt(heartTime[1])),null,null,parseInt(heartBeat)],
                        [new Date(year,mon,day+day_change,parseInt(heartTime[0]),parseInt(heartTime[1])),null,null,parseInt(heartBeat)],
                        [null,null,null,null],
                        [new Date(year,mon,day+day_change,parseInt(heartTime[0]),parseInt(heartTime[1])),null,null,parseInt(heartBeat)],
                        [new Date(year,mon,day+day_change,parseInt(heartTime[0]),parseInt(heartTime[1])),null,null,parseInt(heartBeat)]
                    ]);
                    chart.draw(data, {
                        pointSize: 5
                    });
                }
            }
        }
        checkEventheartrepat++;
    });
}
google.load('visualization', '1', {packages:['corechart'], callback: drawChart});